//------------------------------------------------------------------------------
/// @file
/// @brief    HPCAnswer.hpp の実装 (解答記述用ファイル)
/// @author   ハル研究所プログラミングコンテスト実行委員会
///
/// @copyright  Copyright (c) 2015 HAL Laboratory, Inc.
/// @attention  このファイルの利用は、同梱のREADMEにある
///             利用条件に従ってください

//------------------------------------------------------------------------------

#include "HPCAnswer.hpp"
#include "HPCMath.hpp"

#include "HPCRandom.hpp"
#include <vector>
#include <queue>
#include <stack>

/// プロコン問題環境を表します。
namespace hpc {

	//------------------------------------------------------------------------------
	/// 各ステージ開始時に呼び出されます。
	///
	/// ここで、各ステージに対して初期処理を行うことができます。
	///
	/// @param[in] aStage 現在のステージ。
	void Answer::Init(const Stage& aStage)
	{
	}

	std::vector<std::vector<int>> get_bfs_history(const Field& aField, const Pos& aStart, const Pos& aGoal);
	std::stack<Action> solve_bfs(const Field& aField, const Pos& aStart, const Pos& aGoal);

	int get_near_item_index(const Stage& aStage, const std::vector<int>& v, const Pos& from) {

		float minDist = 1E+10; // 十分大きい値
		int index = -1;

		for (unsigned int i = 0; i < v.size(); i++) {
			Pos target = aStage.items()[v[i]].destination();
			float dist = (float)((target.x - from.x) * (target.x - from.x) + (target.y - from.y) * (target.y - from.y));
			dist = Math::Sqrt(dist);
			if (dist < minDist) {
				index = v[i];
				minDist = dist;
			}
		}

		return index;
	}

	typedef std::pair<int, int> point_t;

	point_t operator+(const point_t& lhs, const point_t& rhs) {
		point_t p = point_t(lhs.first + rhs.first, lhs.second + rhs.second);
		return p;
	}

	bool operator==(const point_t &lhs, const point_t &rhs) {
		return (lhs.first == rhs.first) && (lhs.second == rhs.second);
	}

	bool is_in_field(const Field& aField, const point_t &point) {
		const int y = point.second;
		const int x = point.first;
		return (0 <= x && x < aField.width()) && (0 <= y && y < aField.height());
	}

	std::vector<std::vector<int>> get_bfs_history(const Field& aField, const Pos& aStart, const Pos& aGoal) {
		std::vector<std::vector<int>> memo;
		for (int i = 0; i < aField.height(); ++i) {
			std::vector<int> v(aField.width(), 0);
			memo.push_back(v);
		}
		const point_t points[] = {
			point_t(-1,  0),
			point_t(1,  0),
			point_t(0, -1),
			point_t(0,  1)
		};
		point_t start = point_t(aStart.x, aStart.y);
		point_t	goal = point_t(aGoal.x, aGoal.y);

		std::queue<point_t> q;
		q.push(start);
		memo[start.second][start.first] = 1;

		while (!q.empty()) {
			point_t cur = q.front();
			q.pop();

			if (cur == goal)
				return memo;

			for (const auto &point : points) {
				point_t next = cur + point;
				if (!is_in_field(aField, next))
					continue;
				if (aField.isWall(next.first, next.second))
					continue;
				if (memo[next.second][next.first] == 0) {
					q.push(next);
					memo[next.second][next.first] = memo[cur.second][cur.first] + 1;
				}
			}
		}
		// Error
		return std::vector<std::vector<int>>();
	}

	std::stack<Action> solve_bfs(const Field& aField, const Pos& aStart, const Pos& aGoal) {
		std::vector<std::vector<int>> memo = get_bfs_history(aField, aStart, aGoal);

		const point_t points[] = {
			point_t(-1,  0),
			point_t(1,  0),
			point_t(0, -1),
			point_t(0,  1)
		};
		point_t start = point_t(aStart.x, aStart.y);
		point_t	goal = point_t(aGoal.x, aGoal.y);

		std::stack<Action> act;
		point_t p = goal;

		while (p != start) {
			for (const auto &point : points) {
				point_t next = p + point;
				if (!is_in_field(aField, next))
					continue;
				if (memo[next.second][next.first] == 0)
					continue;
				if (memo[next.second][next.first] < memo[p.second][p.first]) {
					if (point == points[0]) act.push(Action_MoveRight);
					else if (point == points[1]) act.push(Action_MoveLeft);
					else if (point == points[2]) act.push(Action_MoveUp);
					else if (point == points[3]) act.push(Action_MoveDown);
					p = next;
					break;
				}
			}

		}
		return act;
	}

	bool isOfficePass(const Field& aField, const Pos& aStart, const Pos& aGoal) {
		std::vector<std::vector<int>> memo = get_bfs_history(aField, aStart, aGoal);

		const point_t points[] = {
			point_t(-1,  0),
			point_t(1,  0),
			point_t(0, -1),
			point_t(0,  1)
		};

		point_t start = point_t(aStart.x, aStart.y);
		point_t	goal = point_t(aGoal.x, aGoal.y);

		point_t p = goal;
		point_t office = point_t(aField.officePos().x, aField.officePos().y);

		while (p != start) {
			for (const auto &point : points) {
				point_t next = p + point;
				if (!is_in_field(aField, next))
					continue;
				if (memo[next.second][next.first] == 0)
					continue;
				if (memo[next.second][next.first] < memo[p.second][p.first]) {
					p = next;
					if (next == office)
						return true;
					break;
				}
			}
		}
		return false;
	}

	bool isOfficePass(const Field& aField, std::vector<Item> loaded, Item item) {

		for (unsigned int i = 0; i < loaded.size(); ++i) {
			Pos start = loaded.at(i).destination();
			Pos goal = item.destination();
			if (isOfficePass(aField, start, goal)) return true;
		}
		return false;
	}

	void break_point_call() {
		return;
	}

	//std::vector<std::vector<Item>> clustering_kmeans(const ItemCollection items, int k) {
	//	Random random = Random();
	//	std::vector<std::vector<Item>> clusters;
	//	for (int i = 0; i < k; ++i) {
	//		std::vector<Item> v;
	//		clusters.push_back(v);
	//	}
	//	for (int i = 0; i < items.count(); ++i) {
	//		std::vector<Item> *v = &clusters.at(random.randMinMax(0, k - 1));
	//		v->push_back(items[i]);
	//	}
	//	std::vector<std::pair<float, float>> center;
	//	for (int i = 0; i < k; ++i)
	//		center.push_back(std::pair<float, float>(0, 0));
	//	bool dis = true;
	//	const int MAX = 10;
	//	int repeat = 0;
	//	while (dis) {
	//		++repeat;
	//		if (repeat > MAX)
	//			break;
	//		for (int i = 0; i < k; ++i) {
	//			std::vector<Item> *v = &clusters.at(i);
	//			if (v->size() < 1) continue;
	//			float* cx = &center.at(i).first;
	//			float* cy = &center.at(i).second;
	//			float sx = 0, sy = 0;
	//			for (unsigned int j = 0; j < v->size(); ++j) {
	//				Item it = v->at(j);
	//				Pos p = it.destination();
	//				sx += p.x;
	//				sy += p.y;
	//			}
	//			*cx = sx / v->size();
	//			*cy = sy / v->size();
	//		}
	//		dis = false;
	//		for (int i = 0; i < items.count(); ++i) {
	//			Pos p = items[i].destination();
	//			float min = 1E+10;
	//			int index = 0;
	//			for (int j = 0; j < k; ++j) {
	//				float x = center.at(j).first;
	//				float y = center.at(j).second;
	//				float dist = (float)(((float)p.x - x) * ((float)p.x - x) + ((float)p.y - y) * ((float)p.y - y));
	//				dist = Math::Sqrt(dist);
	//				if (min == Math::Min(dist, min)) continue;
	//				min = Math::Min(dist, min);
	//				index = j;
	//			}
	//			for (int h = 0; h < k; ++h) {
	//				std::vector<Item> v = clusters.at(h);
	//				for (unsigned int l = 0; l < v.size(); ++l) {
	//					if (v.at(l).destination() != p || l == index) continue;
	//					dis = true;
	//					v.erase(v.begin() + l);
	//					clusters.at(index).push_back(items[i]);
	//				}
	//			}
	//		}
	//	}
	//	return clusters;
	//}

	//int cluster_of(const std::vector<std::vector<Item>>& clusters, const Item& item) {
	//	for (unsigned int i = 0; i < clusters.size(); ++i) {
	//		std::vector<Item> v = clusters.at(i);
	//		for (unsigned int j = 0; j < v.size(); ++j) {
	//			if (v.at(j).destination() == item.destination())
	//				return i;
	//		}
	//	}
	//	return -1;
	//}

	//------------------------------------------------------------------------------
	/// 各配達時間帯開始時に呼び出されます。
	///
	/// ここで、この時間帯に配達する荷物をトラックに積み込みます。
	/// どの荷物をトラックに積み込むかを決めて、引数の aItemGroup に対して
	/// setItem で荷物番号を指定して積み込んでください。
	///
	/// @param[in] aStage 現在のステージ。
	/// @param[in] aItemGroup 荷物グループ。
	void Answer::InitPeriod(const Stage& aStage, ItemGroup& aItemGroup)
	{
		int cost = 0;
		//int k = (int)((aStage.items().count() + 3) / 4);
		//std::vector<std::vector<Item>> clusters = clustering_kmeans(aStage.items(), k);
		std::vector<Item> loaded;
		// 未配達で積み込んでいない荷物の総重量
		int totalWeight = 0;

		for (int i = 0; i < aStage.items().count(); ++i) {
			// まだ配達されてない荷物かどうか調べる
			if (aStage.getTransportState(i) != TransportState_NotTransported) continue;
			Item item = aStage.items()[i];
			// 時間指定ありの荷物だけ積みこむ
			if (item.period() == aStage.period()) {
				aItemGroup.addItem(i);
				loaded.push_back(item);
				cost += item.weight();
				continue;
			}
			totalWeight += item.weight();
		}

		for (int i = 0; i < aStage.items().count(); ++i) {
			// まだ配達されてない荷物かどうか調べる
			if (aStage.getTransportState(i) != TransportState_NotTransported) continue;
			Item item = aStage.items()[i];
			// 時間指定なしの荷物に限定
			if (item.period() != -1) continue;

			//bool clusterof = false;
			//for (unsigned int j = 0; j < loaded.size(); ++j) {
			//	int n = cluster_of(clusters, item);
			//	if (n == cluster_of(clusters, loaded.at(j))) clusterof = true;
			//}

			//// ステージ前半はクラスタ同じやつだけ運んでみる
			//if (aStage.period() < 3 && clusterof) {
			//	if (item.weight() + cost <= Parameter::TruckWeightCapacity) {
			//		aItemGroup.addItem(i);
			//		loaded.push_back(item);
			//		cost += item.weight();
			//	}
			//	continue;
			//}

			bool wasting = isOfficePass(aStage.field(), loaded, item);

			bool has2space = false;
			int period = aStage.period();
			// 残りの時間で運びきれる => true
			has2space = (Parameter::PeriodCount - period) * Parameter::TruckWeightCapacity > totalWeight;
			// 最初のn時間である => true
			const int n = 3;
			has2space &= (period < (Parameter::PeriodCount - n));
			
			// 無駄じゃなければ積み込む
			// 余裕がないならとにかく積みこんじゃう
			// 何も積んでないならとりあえず積み込んでみる
			if (!wasting || !has2space) {
				if (item.weight() + cost <= Parameter::TruckWeightCapacity) {
					aItemGroup.addItem(i);
					loaded.push_back(item);
					cost += item.weight();
					totalWeight -= item.weight();
				}
				continue;
			}

			//// ステージ後半は積めるだけ積み込んでみる
			//if (item.weight() + cost <= Parameter::TruckWeightCapacity) {
			//	aItemGroup.addItem(i);
			//	loaded.push_back(item);
			//	cost += item.weight();
			//	continue;
			//}
		}
#ifdef DEBUG
		if (!aItemGroup.hasAnyItems()) {
			return;
		}
#endif
	}

	//------------------------------------------------------------------------------
	/// 各ターンでの動作を返します。
	///
	/// @param[in] aStage 現在ステージの情報。
	///
	/// @return これから行う動作を表す Action クラス。
	Action Answer::GetNextAction(const Stage& aStage)
	{
		Truck truck = aStage.truck();

		static bool isPeriodChanged = true;
		static std::stack<std::stack<Action>> acts;
		if (isPeriodChanged) {
			isPeriodChanged = false;
			std::stack<int> transportOrder;
			std::vector<int> indices;
			for (int i = 0; i < aStage.items().count(); i++) {
				// 荷物を配達中で
				if (aStage.getTransportState(i) != TransportState_Transporting) continue;
				// 既に配達済みでもない
				if (!truck.itemGroup().hasItem(i)) continue;
				indices.push_back(i);
			}

			// 最初は営業所にいる
			// 営業所から最も近い配達をする
			Pos current = aStage.field().officePos();
			int index = get_near_item_index(aStage, indices, current);
			transportOrder.push(index);
			for (unsigned int i = 0; i < indices.size(); i++) {
				if (indices.at(i) == index)
					indices.erase(indices.begin() + i);
			}
			current = aStage.items()[index].destination();
			// 以降は配達先から最も近い配達先を探してルートを決定する
			while (!indices.empty()) {
				index = get_near_item_index(aStage, indices, current);
				transportOrder.push(index);
				current = aStage.items()[index].destination();
				for (unsigned int i = 0; i < indices.size(); i++) {
					if (indices.at(i) == index)
						indices.erase(indices.begin() + i);
				}
			}

			// 最後の配達先から営業所まで

			Pos from = aStage.items()[transportOrder.top()].destination();
			Pos to = aStage.field().officePos();
			acts.push(solve_bfs(aStage.field(), from, to));

			while (!transportOrder.empty()) {
				to = from;
				transportOrder.pop();
				from = (transportOrder.empty()) ? aStage.field().officePos() : aStage.items()[transportOrder.top()].destination();
				acts.push(solve_bfs(aStage.field(), from, to));
			}
		}

		std::stack<Action>* act = &acts.top();
		if (act->empty()) {
			acts.pop();
		}
		Action a = act->top();
		act->pop();
		if (act->empty()) {
			acts.pop();
			if (acts.empty())
				isPeriodChanged = true;
		}
		return a;
	}

	//------------------------------------------------------------------------------
	/// 各配達時間帯終了時に呼び出されます。
	///
	/// ここで、この時間帯の終了処理を行うことができます。
	///
	/// @param[in] aStage 現在のステージ。
	/// @param[in] aStageState 結果。Playingならこの時間帯の配達完了で、それ以外なら、何らかのエラーが発生した。
	/// @param[in] aCost この時間帯に消費した燃料。エラーなら0。
	void Answer::FinalizePeriod(const Stage& aStage, StageState aStageState, int aCost)
	{
		if (aStageState == StageState_Failed) {
			// 失敗したかどうかは、ここで検知できます。
#ifdef DEBUG
			break_point_call();
#endif
		}
	}

	//------------------------------------------------------------------------------
	/// 各ステージ終了時に呼び出されます。
	///
	/// ここで、各ステージに対して終了処理を行うことができます。
	///
	/// @param[in] aStage 現在のステージ。
	/// @param[in] aStageState 結果。Completeなら配達完了で、それ以外なら、何らかのエラーが発生した。
	/// @param[in] aScore このステージで獲得したスコア。エラーなら0。
	void Answer::Finalize(const Stage& aStage, StageState aStageState, int aScore)
	{
		if (aStageState == StageState_Failed) {
			// 失敗したかどうかは、ここで検知できます。
#ifdef DEBUG
			break_point_call();
#endif
		}
		else if (aStageState == StageState_TurnLimit) {
			// ターン数オーバーしたかどうかは、ここで検知できます。
#ifdef DEBUG
			break_point_call();
#endif
		}
	}
}

//------------------------------------------------------------------------------
// EOF
